# Install script for ubuntu 18

sudo apt-get update
sudo apt-get install build-essential git

git clone https://github.com/nsg-ethz/mini_internet_project.git

# Install Docker
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

sudo usermod -aG docker adv-net

# Install OpenvSwitch
sudo apt-get install openvswitch-switch
# Install OpenVPN
sudo apt-get install openvpn

# Install thing for the multicast exercise
sudo apt-get install -y smcroute vlc

# Download the docker images needed
docker pull thomahol/d_host
docker pull thomahol/d_router
docker pull thomahol/d_ssh
docker pull thomahol/d_switch

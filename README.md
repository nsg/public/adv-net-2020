# Advanced Topics in Communication Networks 2020

Welcome to the Advanced Topics in Communication Networks Repository!
Here you will find the weekly exercises and instructions on how to run these exercises in a virtual environment.

## How to start?

To make your life easier, we provide each of you with access to one VM where all the necessary tools and software are pre-installed.

### Access your own VM

To access your VM, you will use SSH. SSH is a UNIX based command-line interface and protocol for securely getting access
to a remote computer. It is widely used by system administrators to control network devices
and servers remotely. An SSH client is available by default on any Linux and MAC installation
through the Terminal application. For Windows 10 users, there is SSH functionality avaliable in the Command Prompt. For other Windows users, a good and free SSH client is [PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/).
Once you have installed an SSH client, use the following command to connect yourself to your
VM:

```
ssh -p X p4@moonshine.ethz.ch
```

where X = 2000 + your student number for this lecture that we have sent you by email.
For instance if you are the student 7, use the following command

```
ssh -p 2007 p4@moonshine.ethz.ch
```

If you cannot connect to your VM,
please report it immediately during the exercise session or in [Moodle](https://moodle-app2.let.ethz.ch/mod/forum/view.php?f=22500). If you want to simplify
the access to your VM (optional), please use SSH key authentication, but do not change your
password. If you want to download an entire directory (e.g. the configs directory) from your
VM to the current directory of your own machine, you can use scp:

```
> scp -r -P X p4@moonshine.ethz.ch:~/path to the directory .
```

where X = 2000 + group number. On Windows, you can use WinSCP7 to do that. Note the
dot at the end of the command and the capitalized P.

### Remote Development

We need to be able to open and edit remote files in our local code editor to have a smooth development cycle. This way, we can work with our code locally, and execute it remotely without any friction. Below, we explain how to achieve this for Visual Studio Code as an example.

1) Download Visual Studio Code.
2) Access VS Code, in the left-side dock enter `Extensions` menu.  
3) Install `Remote - SSH` extension.
4) In the pop-up prompt: enter your SSH credentials as you did for the VM access.
5) Your "Remote Directory" should appear in the Explorer.
6) [Optional] Go to Extensions menu again, and install `P4 Language Extension` in your remote machine for highlights and syntax check.

For VS Code, you can find further information [here](https://code.visualstudio.com/docs/remote/ssh).
Many other text editors provide a similiar functionality. For example, Atom has the [remote-synch](https://atom.io/packages/remote-sync) package to upload and download files directly from inside Atom.

If you are already familiar with remote development, continue with your favourite code editor.


#### VM Credentials

During the lecture, we will have two types of exercises. First, we will have p4-based exercises. Then, we will have exercises based on the mini-Internet.
We will use two different users for the two types of exercises. For the p4-based exercises please use the login `p4`, and for the mini-Internet-based exercises please use the login `mini_internet`.
We have sent you your password by email. It is the same for both logins. If you have not received your password please let us know immediately.

### VM Contents

The VM is based on a Ubuntu 16.04.05 and after building it contains:

* The suite of P4 Tools ([p4lang](https://github.com/p4lang/), [p4utils](https://github.com/nsg-ethz/p4-utils/tree/master/p4utils), etc)
* Text editors with p4 highlighting (sublime, atom, emacs, vim)
* [Wireshark](https://www.wireshark.org/)
* [Mininet](http://mininet.org/) network emulator
* All the tools required to run a [mini-Internet](https://github.com/nsg-ethz/mini_internet_project)


## Exercises

In this section we provide the links to the weekly exercises. There are two types of exercises. The first ones will be P4-based whereas later during the semester you will have mini-Internet-based exercises (the same kind of mini-Internet that you used in the Communication Networks lecture).
To get the exercises ready in your VM, please clone this repository both times; one in the `p4` user home directory and one in the `mini_internet` user home directory. We illustrate how to do that for the `p4` user.

```
cd /home/p4/
git clone https://gitlab.ethz.ch/nsg/public/adv-net-2020
```

Update local repository to get new tasks and solutions
Remember to pull this repository before every exercise session:

```
cd /home/p4/adv-net-2020
git pull https://gitlab.ethz.ch/nsg/public/adv-net-2020
```

### P4 Exercise Sessions

#### Week 1: Introduction to P4 (15/09/2020)

* [Introduction to P4](https://gitlab.ethz.ch/nsg/public/adv-net-2020/-/tree/master/01-Tutorials)

#### Week 2: MPLS (22/09/2020)

 * [MPLS Data Plane](./02-MPLS)

#### Week 3 & 4: RSVP (29/09/2020 - 06/10/2020)

 * [RSVP Controller](./03-RSVP)

#### Week 5: Load balancing (13/10/2020)

 * [Load Balancing](./04-Load_balancing)

#### Week 6: Traffic control (20/10/2020)

 * [Load Balancing](./05-Traffic_control)

#### Week 7 & 8: BGP VPN with MPLS (27/10/2020 - 3/11/2020)

 * [MPLS_VPN_VRF](./07-MPLS_VPN_VRF)

#### Week 9: IP Fast Reroute to LFA (10/11/2020)

 * [Fast_Reroute](./08-Fast_reroute)

## Where to ask questions?

If you have questions, you can ask us during the exercise sessions (every Tuesday at 4.15pm) using the Zoom in-meeting chat or in the #exercise channel on [slack](adv-net20.slack.com). You can post more general questions on the [moodle](https://moodle-app2.let.ethz.ch/) forum.  
Please do **not** ask us questions by email.

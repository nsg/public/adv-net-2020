# Linux Traffic Control

In this exercise, we will learn how to use a very powerful Linux tool, the Linux TC toolkit. Linux TC is part of the `iproute2` package and is a set of user-space tools used to configure the Linux interface's packet schedulers, shapers, policers. Today's exercise will consist of a big part of self-learning where you will have to read about Linux traffic control. After that, we introduce you to some intuitive examples. Finally, we introduce a problem and you have to solve it using what you have learnt. Thus, for this exercise we will not use `P4`. This document is split in the following sections:

* [Traffic control summary](#traffic-control)
* [Learning through examples](#learning-through-examples)
* [Real scenario exercise](#real-scenario-quality-of-service)

## Traffic Control

Traffic Control encompasses the sets of mechanisms and operations by which packets are queued for transmission/reception on a network interface. The operations include enqueuing, policing, classifying, scheduling, shaping, and dropping.

Traffic Control is the name given to the sets of queuing systems and mechanisms by which packets are received and transmitted on a router. This includes deciding which (and whether) packets to accept at what rate on the input of an interface and determining which packets to transmit in what order at what rate on the output of an interface.

In the overwhelming majority of situations, traffic control consists of a single queue which collects entering packets and dequeues them as quickly as the hardware can accept them. This sort of queue is a First-In First-Out (FIFO).

NOTE: The default qdisc under Linux is the pfifo_fast (explained later), which is slightly more complex than the FIFO.

> Because network links typically carry data in a serialized fashion, a queue is required to manage the outbound data packets.

## Why Use Traffic Control?

Packet-switched networks differ from circuit-based networks in one very important regard. A packet-switched network itself is stateless.

The weakness of this statelessness is the lack of differentiation between types of flows. In simplest terms, traffic control allows an administrator to queue packets differently based on its attributes. It can even be used to simulate the behavior of a circuit-based network. This introduces statefulness into the stateless network.

Common Traffic Control Solutions:

* Limit total bandwidth to a known rate; TBF (tocken bucket filter), HTB (hierarchical tocken bucket) with child class(es).
* Limit the bandwidth of a particular user, service or client; HTB classes and classifying with a filter traffic.
* Reserve bandwidth for a particular application or user; HTB with children classes and classifying.
* Prefer latency sensitive traffic; PRIO inside an HTB class.
* Allow equitable distribution of unreserved bandwidth; HTB with borrowing.
* Ensure that a particular type of traffic is dropped; policer attached to a filter with a drop action.

## Queues

Queues form the backdrop for all of traffic control, and are the integral concept behind scheduling. A queue is a location (or buffer) containing a finite number of items waiting for an action or service. In networking, a queue is the place where packets wait to be transmitted. In the simplest model, packets are transmitted in a first-come first-serve basis (FIFO).

Without any other mechanisms, a queue doesn't offer any promise for traffic control. There are only two interesting actions in a queue. Anything entering a queue is enqueued into the queue. To remove an item from a queue is to dequeue that item.

A queue becomes much more interesting when coupled with other mechanisms which can delay packets, rearrange, drop and prioritize them in multiple queues. A queue can also use sub-queues, which allow for complexity in a scheduling operation.

## Tokens and Buckets

Two of the key underpinnings of a shaping mechanism are the interrelated concepts of tokens and buckets.

In order to control the rate of dequeuing, an implementation can count the number of packets or bytes dequeued as each item is dequeued, although this requires complex usage of timers and measurements to limit accurately. Instead of calculating the current usage and time, one method, used widely in traffic control, is to generate tokens at a desired rate, and only dequeue packets or bytes if a token is available.

Consider the analogy of an amusement park ride with a queue of people waiting to experience the ride. Let's imagine a track on which carts traverse a fixed track. The carts arrive at the head of the queue at a fixed rate. In order to enjoy the ride, each person must wait for an available cart. The cart is analogous to a token and the person is analogous to a packet. Again, this mechanism is a rate-limiting or shaping mechanism. Only a certain number of people can experience the ride in a particular period.

To extend the analogy, imagine an empty line for the amusement park ride and a large number of carts sitting on the track ready to carry people. If a large number of people entered the line together many (maybe all) of them could experience the ride because of the carts available and waiting. The number of carts available is a concept analogous to the bucket. A bucket contains a number of tokens and can use all of the tokens in the bucket at any speed (burst).

And to complete the analogy, the carts on the amusement park ride (our tokens) arrive at a fixed rate and are only kept available up to the size of the bucket. So, the bucket is filled with tokens according to the rate, and if the tokens are not used, the bucket can fill up. If tokens are used the bucket will not fill up. Buckets are a key concept in supporting bursty traffic such as HTTP.

The TBF qdisc is a classical example of a shapper (the section on TBF includes a diagram which may help to visualize the token and bucket concepts). The TBF generates rate tokens and only transmits packets when a token is available. Tokens are a generic shaping concept.

Very predictable regular traffic can be handled by small buckets. Larger buckets may be required for burstier traffic, unless one of the desired goals is to reduce the burstiness of the flows.

_**In summary, tokens are generated at rate, and a maximum of a bucket's worth of tokens may be collected. This allows bursty traffic to be handled, while smoothing and shaping the transmitted traffic.**_

The concepts of tokens and buckets are closely interrelated and are used in both TBF (one of the classless qdiscs) and HTB (one of the classful qdiscs).

## Traditional Elements of Traffic Control

### Shaping

Shapers delay packets to meet a desired rate.

Shaping is the mechanism by which packets are delayed before transmission in an output queue to meet a desired output rate.

Shapers attempt to limit or ration traffic to meet but not exceed a configured rate. As a side effect, shapers can smooth out bursty traffic.

One of the advantages of shaping bandwidth is the ability to control latency of packets. The underlying mechanism for shaping to a rate is typically a token and bucket mechanism.

### Scheduling

Schedulers arrange and/or rearrange packets for output.

Scheduling is the mechanism by which packets are arranged (or rearranged) between input and output of a particular queue. The overwhelmingly most common scheduler is the FIFO (first-in first-out) scheduler. From a larger perspective, any set of traffic control mechanisms on an output queue can be regarded as a scheduler, because packets are arranged for output.

Other generic scheduling mechanisms attempt to compensate for various networking conditions. A fair queuing algorithm (see SFQ) attempts to prevent any single client or flow from dominating the network usage. A round-robin algorithm gives each flow or client a turn to dequeue packets.

### Classifying

Classifiers separate traffic into queues.

Classifying is the mechanism by which packets are separated for different treatment, possibly different output queues. During the process of accepting, routing and transmitting a packet, a networking device can classify the packet a number of different ways. Classification can include marking the packet, which usually happens on the boundary of a network under a single administrative control or classification can occur on each hop individually.

The Linux model allows for a packet to cascade across a series of classifiers in a traffic control structure and to be classified in conjunction with policers.

### Policing

Policers measure and limit traffic in a particular queue.

Policing, as an element of traffic control, is simply a mechanism by which traffic can be limited. Policing is most frequently used on the network border to ensure that a peer is not consuming more than its allocated bandwidth. A policer will accept traffic to a certain rate, and then perform an action on traffic exceeding this rate. A rather harsh solution is to drop the traffic, although the traffic could be reclassified instead of being dropped.

A policer is a yes/no question about the rate at which traffic is entering a queue. If the packet is about to enter a queue below a given rate, take one action (allow the enqueuing). If the packet is about to enter a queue above a given rate, take another action. Although the policer uses a token bucket mechanism internally, it does not have the capability to delay a packet as a shaping mechanism does.

### Dropping

Dropping discards an entire packet, flow or classification.

Dropping a packet is a mechanism by which a packet is discarded.

### Marking

Marking is a mechanism by which the packet is altered.

Traffic control marking mechanisms install a DSCP on the packet itself (for example for IP packets they modify the TOS field), which is then used and respected by other routers inside an administrative domain (usually for DiffServ).

## Components of Linux Traffic Control

| TC Element | Linux Component |
|-|-|
| shaping | The "class" offers shaping capabilities |
| scheduling | A "qdisc" is a scheduler. Schedulers can be simple such as the FIFO or complex, containing classes and other qdiscs, such as HTB. |
| classifying | The "filter" object performs the classification through the agency of a classifier object. |
| policing | A "policer" exists in the Linux traffic control implementation only as part of a "filter". |
| dropping | To "drop" traffic requires a "filter" with a "policer" which uses drop as an action. |
| marking | The dsmark "qdisc" is used for marking. |

### qdisc

Simply put, a qdisc is a scheduler. Every output interface needs a scheduler of some kind, and the default scheduler is a FIFO. Other qdiscs available under Linux will rearrange the packets entering the scheduler's queue in accordance with that scheduler's rules.

The qdisc is the major building block on which all of Linux traffic control is built, and is also called a queuing discipline.

The classful qdiscs can contain classes, and provide a handle to which to attach filters.

The classless qdiscs can contain no classes, nor is it possible to attach a filter to a classless qdisc. Because a classless qdisc contains no children of any kind, there is no utility to classifying. This means that no filter can be attached to a classless qdisc.

### class

Classes only exist inside a classful qdisc (e.g., HTB). Classes are immensely flexible and can always contain either multiple children classes or a single child qdisc. There is no prohibition against a class containing a classful qdisc itself, which facilitates tremendously complex traffic control scenarios.

Any class can also have an arbitrary number of filters attached to it, which allows the selection of a child class or the use of a filter to reclassify or drop traffic entering a particular class.

A leaf class is a terminal class in a qdisc. It contains a qdisc (default FIFO) and will never contain a child class. Any class which contains a child class is an inner class (or root class) and not a leaf class.

### filter

The filter provides a convenient mechanism for gluing together several of the key elements of traffic control. The simplest and most obvious role of the filter is to classify packets. Linux filters allow the user to classify packets into an output queue with either several different filters or a single filter.

* A filter must contain a classifier phrase.
* A filter may contain a policer phrase.

Filters can be attached either to classful qdiscs or to classes, however the enqueued packet always enters the root qdisc first. After the filter attached to the root qdisc has been traversed, the packet may be directed to any subclasses (which can have their own filters) where the packet may undergo further classification.

### classifier

Filter objects, which can be manipulated using tc, can use several different classifying mechanisms, the most common of which is the u32 classifier. The u32 classifier allows the user to select packets based on its attributes.

The classifiers are tools which can be used as part of a filter to identify characteristics of a packet or a packet's metadata. The Linux classifier object is a direct analogy to the basic operation and elemental mechanism of traffic control classifying.

### policer

This elemental mechanism is only used in Linux traffic control as part of a filter. A policer calls one action above and another action below the specified rate. Clever use of policers can simulate a three-color meter.

Although both policing and shaping are basic elements of traffic control for limiting bandwidth usage a policer will never delay traffic. It can only perform an action based on specified criteria.

### drop

This basic traffic control mechanism is only used in Linux traffic control as part of a policer. Any policer attached to any filter could have a drop action.

> The only place in the Linux traffic control system where a packet can be explicitly dropped is a policer. A policer can limit packets enqueued at a specific rate, or it can be configured to drop all traffic matching a particular pattern.

There are, however, places within the traffic control system where a packet may be dropped as a side effect. For example, a packet will be dropped if the scheduler employed uses this method to control flows.

Also, a shaper or scheduler which runs out of its allocated buffer space may have to drop a packet during a particularly bursty or overloaded period.

### handle

Every class and classful qdisc requires a unique identifier within the traffic control structure. This unique identifier is known as a handle and has two constituent members, a major number and a minor number. These numbers can be assigned arbitrarily by the user in accordance with the following rules.

The numbering of handles for classes and qdiscs:

major => The user may use an arbitrary numbering scheme, however all objects in the traffic control structure with the same parent must share a major handle number. Conventional numbering schemes start at 1 for objects attached directly to the root qdisc.

minor => This parameter unambiguously identifies the object as a qdisc if minor is 0. Any other value identifies the object as a class. All classes sharing a parent must have unique minor numbers.

The handle is used as the target in classid and flowid phrases of tc filter statements.

## Classless Queuing Disciplines (qdiscs)

Each of these queuing disciplines can be used as the primary qdisc on an interface, or can be used inside a leaf class of a classful qdiscs. These are the fundamental schedulers used under Linux. Note that the default scheduler is the pfifo_fast.

### FIFO, First-In First-Out (pfifo and bfifo)

The FIFO algorithm forms the basis for the default qdisc on all Linux network interfaces (pfifo_fast). It performs no shaping or rearranging of packets. It simply transmits packets as soon as it can after receiving and queuing them. This is also the qdisc used inside all newly created classes until another qdisc or a class replaces the FIFO.

<p align="center">
<img src="images/fifo.png">
</p>

A real FIFO qdisc must, however, have a size limit (a buffer size) to prevent it from overflowing in case it is unable to dequeue packets as quickly as it receives them. Linux implements two basic FIFO qdiscs, one based on bytes, and one on packets. Regardless of the type of FIFO used, the size of the queue is defined by the parameter limit. For a pfifo the unit is understood to be packets and for a bfifo the unit is bytes.

### pfifo_fast, the default Linux qdisc

The pfifo_fast qdisc is the default qdisc for all interfaces under Linux. Based on a conventional FIFO qdisc, this qdisc also provides some prioritization. It provides three different bands (individual FIFOs) for separating traffic. The highest priority traffic (interactive flows) are placed into band 0 and are always serviced first. Similarly, band 1 is always emptied of pending packets before band 2 is dequeued.

<p align="center">
<img src="images/pfifo.png">
</p>

### SFQ, Stochastic Fair Queuing

The SFQ qdisc attempts to fairly distribute opportunity to transmit data to the network among an arbitrary number of flows. It accomplishes this by using a hash function to separate the traffic into separate (internally maintained) FIFOs which are dequeued in a round-robin fashion. Because there is the possibility for unfairness to manifest in the choice of hash function, this function is altered periodically.

<p align="center">
<img src="images/sfq.png">
</p>

### TBF, Token Bucket Filter

This qdisc is built on tokens and buckets. It simply shapes traffic transmitted on an interface. To limit the speed at which packets will be dequeued from a particular interface, the TBF qdisc is the perfect solution. It simply slows down transmitted traffic to the specified rate.

Packets are only transmitted if there are sufficient tokens available. Otherwise, packets are deferred. Delaying packets in this fashion will introduce an artificial latency into the packet's round trip time.

<p align="center">
<img src="images/tbf.png">
</p>

## Classful Queuing Disciplines (qdiscs)

The classful queuing disciplines can have filters attached to them, allowing packets to be directed to particular classes and subqueues.

### HTB, Hierarchical Token Bucket

HTB uses the concepts of tokens and buckets along with the class-based system and filters to allow for complex and granular control over traffic. With a complex borrowing model, HTB can perform a variety of sophisticated traffic control techniques. One of the easiest ways to use HTB immediately is that of shaping.

By understanding tokens and buckets or by grasping the function of TBF, HTB should be merely a logical step. This queuing discipline allows the user to define the characteristics of the tokens and bucket used and allows the user to nest these buckets in an arbitrary fashion. When coupled with a classifying scheme, traffic can be controlled in a very granular fashion.

#### Shaping

One of the most common applications of HTB involves shaping transmitted traffic to a specific rate.

All shaping occurs in leaf classes. No shaping occurs in inner or root classes as they only exist to suggest how the borrowing model should distribute available tokens.

#### Borrowing

A fundamental part of the HTB qdisc is the borrowing mechanism. Children classes borrow tokens from their parents once they have exceeded rate. A child class will continue to attempt to borrow until it reaches ceil, at which point it will begin to queue packets for transmission until more tokens/ctokens are available.

### PRIO, priority scheduler

The PRIO classful qdisc works on a very simple precept. When it is ready to dequeue a packet, the first class is checked for a packet. If there's a packet, it gets dequeued. If there's no packet, then the next class is checked, until the queuing mechanism has no more classes to check.

## Where to find more information

You can find a plethora of TC pages we collected for you, the ones we think are more interesting are as follows:

* [Main linux TC man page](https://man7.org/linux/man-pages/man8/tc.8.html)
* [u32 filters man page](https://man7.org/linux/man-pages/man8/tc-u32.8.html)
* [TC HOWTO](http://linux-ip.net/articles/Traffic-Control-HOWTO/)
* [Another HOWTO](https://tldp.org/HOWTO/Adv-Routing-HOWTO/)
* [TC examples](https://wiki.archlinux.org/index.php/Advanced_traffic_control)

Also in general you can use the man-page for every qdisc and class. For example for HTB:

```
man tc-htb
```

## Most basic commands:

Create a root qdisc:
```
tc qdisc add dev <DEV> handle 1: root <QDISC> [PARAMETERS]
```

Create a non-root qdisc:
```
tc qdisc add dev <DEV> parent <PARENTID> handle <HANDLEID> <QDISC> [PARAMETER]
```

Create a class:
```
tc class add dev <DEV> parent <PARENTID> classid <CLASSID> <QDISC> [PARAMETER]
```

Delete all qdiscs and classes:
```
tc qdisc del root dev <DEV>
```

PARAMETER: parameter specific to the qdisc attached


DEV: interface at which packets leave, e.g. eth1

PARENTID: id of the class to which the qdisc is attached e.g. X:Y

HANDLEID: unique id, by which this qdisc is identified e.g. X:

CLASSID: unique id, by which this class can be identified e.g. X:Y

PARAMETER: parameter specific to the qdisc attached

QDISC: type of the qdisc attached

| qdisc | description | type |
|-|-|-|
| pfifo_fast | simple first in first out qdisc | classless |
| TBF | Token Bucket Filter, limits the packet rate | classless |
| SFQ | Stochastic Fairness Queueing, devides traffic into queues and sends packets in a round robin fashion | classless |
| PRIO | allows packet prioritisation | classful |
| CBQ | allows traffic shaping, very complex | classful |
| HTB | derived from CBQ, but much easier to use | classful |

### Monitoring and Debugging

The commands below can be used to monitor the queuing disciplines and classes that are already in use in a device interface.
```
tc -s -d -p  qdisc show dev <intf-name>
tc -s -d -p  class show dev <intf-name>
```

Example output:

```
tc -s -d -p  qdisc show dev router-eth3
qdisc htb 1: root refcnt 2 r2q 10 default 20 direct_packets_stat 5579 ver 3.17 direct_qlen 1000
 Sent 292322242 bytes 193934 pkt (dropped 15961, overlimits 59006 requeues 0)
 backlog 131638b 64p requeues 0
qdisc pfifo 10: parent 1:10 limit 100p
 Sent 0 bytes 0 pkt (dropped 0, overlimits 0 requeues 0)
 backlog 0b 0p requeues 0
 ```

## Learning Through Examples

The best way to understand how linux TC works and what it can do to traffic is through examples. During the following examples and exercise we will use following topology:

<p align="center">
<img src="images/week6_topo.png">
</p>

Furthermore, unlike in the `P4` exercises where we used `p4-utils` and `mininet` to create topologies, this time we will
use Linux tools (specifically the `ip` toolkit) instead. For these examples and exercises we provide you many useful scripts
(have a look at all of them). Brief description is as follows:

1. Topology creation and deletion:
    * `scripts/run_topo.sh`: starts the topology using Linux namespaces and virtual ethernet links. With that script you can see how Linux virtual networks are created. Indeed, Mininet,
    the Python wrapper we used to create all the `P4` exercise topologies uses these tools behind the curtain.
    * `scripts/clean_topo.sh`: stops the network by simply deleting all the Linux namespaces. This will also automatically delete all the interfaces that were associated with those namespaces.

2. Linux TC scripts to assign the right qdiscs to `router-eth3` interface. Also we provide a clean script:
    * `tc-examples/simple-rate-limit.sh`, `tc-examples/fair-rate-limit.sh`, `tc-examples/prio-rate-limit.sh`: TC scripts that install the required qdiscs and classes for the examples below.
    * `scripts/clean_qos.sh`: removes all the root qdiscs from all hte interfaces. In theory, we should only have qdiscs in `router-eth3`, thus for the other links it will prompt a warning, just ignore it.

3. Automatic Topology set up and traffic generation:
    * `scripts/traffic-example.sh`: utility script that uses tmux to automatically create a set of panels. The script will automatically launch the topology for you and start 1 TCP and 1 UDP flow from h1-> server and h2-> server. You can modify/change the traffic commands afterwards.
    * `scripts/traffic-all-classes.sh`: Similarly, it starts the topology, but this time it generates the 5 classes of traffic we want to use to solve our exercise.


Note: during this exercise, we will access namespaces using `ip netns` command instead of `mx`. To get a shell into a namespace run:

```
sudo ip netns exec <node> bash
```

To run a command:

```
sudo ip netns exec h1 <command>
```

Let us dive into the walkthrough examples!

### Rate limiting

In this first example, we will show that Linux namespaces and virtual interfaces have no sense
of link bandwidth. That means that unless you rate-limit the traffic, `veth` are just limited by
software limits. During the `P4` exercises, every time we limited a link's bandwidth we were actually
using a Linux `TC` traffic shaper without you even noticing.


Note: we strongly recommend you to use `tmux` during this exercise. Majority of utility scripts use tmux to generate many terminals.
Tmux is already installed in the vm. You only have to type `tmux` and a session will be started. With tmux you can multiplex
many terminals inside one window. (The default tmux special key is Control+a).

First, we will show that without adding anything to the interfaces the rate is not limited.

1. Run the topology (clean if needed):

```
scripts/run_topo.sh
```

2. Get two shells, one into `h1` and one into `server`. And start an iperf.

```
$ sudo ip netns exec server bash
$ iperf -s -p 5001 -i 1
```

```
$ sudo ip netns exec h1 bash
$ iperf -c 10.0.3.2 -i 1 -t 10
```

3. You will see that the rate is not limited and you get Gbps rates.

4. Run the `tc-examples/simple-rate-limit.sh`. This script will install an `htb`
   rate limiter(10mbps) in the `router-eth3` interfaces. Furthermore, it will attach
   a 1000 packet `pfifo` at the leaf.

5. Repeat the iperf test. After installing the htb rate shaper you should see:

<p align="center">
<img src="images/rate-limiting.png">
</p>

However, just rate-limiting traffic might not be the best when mixing different
types of traffic that have different requirements. We will now see a set of examples in which some traffic types will have a huge impact on others.


To make it easier now, we will use one of our utility scripts, which will start the topology and two flows automatically. For that you must be inside a `tmux` session:

1. Run `./scripts/traffic-example.sh`. It will take 3-4 seconds until traffic starts. By default
it sets the `rate-limiting` qdiscs. See line 27. It will send one TCP and UDP (15mbps) flows.

```
./scripts/traffic-example.sh
```

2. You should observe how the UDP flow almost kills the TCP flow. This happens because, UDP will send
traffic no matter what at a higher rate than the interface supports. That ends up killing the TCP
flow which does not get its fair share. Also the router's queue ends up being full at 1000 packets, increasing
the delay a lot. You can see that the queue is full by checking `router-eth3` qdiscs:

```
$ sudo ip netns exec router tc -s -d -p qdisc show dev router-eth3
qdisc htb 5: root refcnt 2 r2q 10 default 1 direct_packets_stat 0 ver 3.17 direct_qlen 1000
 Sent 85934174 bytes 56849 pkt (dropped 7008, overlimits 112204 requeues 0)
 backlog 1528700b 1000p requeues 0
qdisc pfifo 10: parent 5:1 limit 1000p
 Sent 85934174 bytes 56849 pkt (dropped 7008, overlimits 0 requeues 0)
 backlog 1528700b 1000p requeues 0
 ```

3. To see how bad the delay gets, you can stop the TCP flow, and start a ping to the `server`:

```
ping 10.0.3.2
```

```
# ping 10.0.3.2
PING 10.0.3.2 (10.0.3.2) 56(84) bytes of data.
64 bytes from 10.0.3.2: icmp_seq=3 ttl=63 time=1225 ms
64 bytes from 10.0.3.2: icmp_seq=5 ttl=63 time=1278 ms
64 bytes from 10.0.3.2: icmp_seq=12 ttl=63 time=1255 ms
64 bytes from 10.0.3.2: icmp_seq=16 ttl=63 time=1398 ms
64 bytes from 10.0.3.2: icmp_seq=17 ttl=63 time=1406 ms
64 bytes from 10.0.3.2: icmp_seq=19 ttl=63 time=1472 ms
64 bytes from 10.0.3.2: icmp_seq=20 ttl=63 time=1429 ms
64 bytes from 10.0.3.2: icmp_seq=21 ttl=63 time=1396 ms
^C
--- 10.0.3.2 ping statistics ---
22 packets transmitted, 8 received, 63% packet loss, time 21301ms
rtt min/avg/max/mdev = 1225.411/1357.843/1472.662/85.091 ms, pipe 2
```

You can see that the pings that make it through (63% get dropped) have a delay of 1.3 seconds. This happens
because the `pfifo` queue gets filled with 1000 packets, and thus it takes time until ping packets are dequeued.

4. One way of reducing this latency without modifying almost anything is by reducing the `pfifo` queue length. For example
by modifying the following line in the `./tc-examples/simple-rate-limit.sh`.

```
sudo ip netns exec router tc qdisc add dev router-eth3 parent 5:1 handle 10: pfifo limit 100
```

We will now show how by adding some simple `qdiscs` or `classes` we can mitigate the problems shown above.

### Fair queueing

In the following example we will see how we can fix almost all the previous problems by just changing
the `htb` leaf node, from a `pfifo` to a `sfq` (stochastic fair queueing).

For this, you can reuse the previous tmux panes, or you can run the `./scripts/traffic-example.sh` script again. In
any case you will have to modify the queueing disciplines:

1. Remove all the queue discs. Bandwidth will go back to gbps:

```
./scripts/clean_qos.sh
```

2. Run the fair-queueing script `./tc-examples/fair-rate-limit.sh`. If you have a look at the script, we replaced the `pfifo` by:

```
sudo ip netns exec router tc qdisc add dev router-eth3 parent 5:1 handle 10: sfq perturb 10 limit 64 quantum 10000
```

This adds a stochastic fair-queue at the end of the htb classfu qdisc. This `sfq` has 1024 queues,
with length 64 packets, and the hash function is modified every 10 seconds (perturb).

After you installed the `sqf` you should see that now UDP and TCP traffic get a fair share of the channels bandwidth (start the iperfs again, if the finished). Furthermore,
if you send a parallel `ping` while the other flows are in the network, delay should be way smaller than before.

UDP and TCP share bandwidth. You can see now how the UDP and TCP flows get approximately half of the links banwdith.

<p align="center">
<img src="images/udp-tcp-share.png">
</p>

Ping does not get completely blocked by heavy hitters. The lower two terminals show how a 15mps (~9.5 mbps received) does not dramatically affect
the latency of other flows. You can see in the first terminal that latency went down from 1.3 s to ~10ms.

<p align="center">
<img src="images/ping-share.png">
</p>

### Priority queueing

Even though fair queuing technically fixed all the problems we had, fair queueing is not the solution for everything. For example, if you have a big amount of flows, they will
all be scheduled in a round robin fashion, meaning that no flow (or traffic type) has any priority over others.

We will now show how we can use `htb` children classes with priorities and filter some specific traffic type such that it uses a prioritized queue. With that, we will be able to
further decrease `ping`'s delay by giving it priority over all other traffic.

Have a look at `tc-examples/prio-rate-limit`, you can see the following:

1. Two `htb` child classes connected to an `htb` parent. The first class (`1:10`) has a priority of one and a guaranteed bandwidth of 1Mbps. Class two (`1:20`) has a priority of
2 and a guaranteed bandwidth of 9Mbps.
2. The root `htb` qdisc defaults to its child number 20. (see line 5)
3. Each child gets a qdisc attached. The first a `pfifo` and the second a `sqf`.
4. Finally we defined a filter rule with parent `1:` (this placed after the root), that matches all the packets with IP protocol = 1 (icmp, ping) and sends them to the `1:10` child. Which is the `htb` with priority 1.

Note: you can find more info about filter rules [here](https://man7.org/linux/man-pages/man8/tc-u32.8.html), also [this pdf (page 11)](http://tcn.hypert.net/tcmanual.pdf) has some good examples of how to use combined rules.

Lets apply this qdiscs (change them or start topo again):

```
./scripts/clean_qos.sh
./tc-examples/prio-rate-limit.sh
```

Now lets start a UDP flow and a ping:

<p align="center">
<img src="images/prio-ping.png">
</p>

You can see that now ping's delay is even lower, 0.061ms!!! while the UDP flow is sending at max speed. Also you can check in the upper right terminal how the `pfifo`
qdisc has sent 31 packets (pings), and the `sfq` has sent 26413 packets (the UDP). You can also see that the `sfq` is dropping some packets due to the fact we
are pushing a `15mbps` UDP flow.


## Real scenario quality of service

Finally, its time to solve a small realistic exercise. For this exercise, we will use the topology
we introduced [earlier](#learning-through-examples). The topology consists of two hosts connected to the internet
via a single router. The router is rate limiting the upstream link at 10Mbps, and that cant not be modified since it was hardcoded by
your ISP into the device. `class-rate-limit.sh` its your configuration starting point. To solve this exercise you will have to add the needed qdiscs and classes as children.
of that root `htb`.


H1 and H2 are running different services that have some requirements. Your goal is to use TC such that those requirements are met.

Their characteristics are as follows:

| Service Name | Requirement | Priority| Attribute |
|-|-|-|-|
| SSH | Very low latency, low bandwidth | 1 |  TOS=1 |
| VoIP | Low latency, medium bandwidth | 2 | TOS=2 |
| Zoom | Low latency, high bandwidth | 3 | TCP DPORT=7070 |
| Netflix | High bandwidth, medium latency | 4 | TCP DPORT=6789 |
| Backup | ----------------- | 5 | UDP DPORT=5000 |
| Other  | ----------------- | 6 | Otherwise      |


You will define some traffic control rules at `router` such that the rate of traffic from each of these hosts is controlled and also the different services within each host
are prioritized based on their characteristics. Furthermore, you have to ensure fairness among traffic from the same type and/or different host. To solve the exercise, use
extend the `class-rate-limit.sh` script.

To make your life a bit easier we provide you an additional `tmux` script (`./scripts/traffic-all-classes.sh`) to start the topology, load the `class-rate-limit.sh` script and start 1 flow for each of the traffic types (by default from h1 only). If you run the script with only the default rate limiter, you will see that no requirement is met, with exception
of the backup UDP traffic that actually takes almost all the bandwidth available:

```
./scripts/traffic-all-classes.sh
```

In the image below you can see that the tmux script has opened 10 terminals. The first two ones are used to start the topology, and you can then use them to add/remove qdiscs. At
the second row we have 2 different pings, the first one represents the `SSH` traffic, the second one represents the `VoIP` traffic. The main difference between them is the packet size and
send frequency, and TOS field. The following three rows represent (client on the left, server on the right), the Zoom, netflix and backup (udp) traffic respectively. Have a look at the `scripts/traffic-all-classes` to see how the pings and iperfs are started.

If you have a look at delays and bandwidth, you can see that non of the requirements is met. Both latency-sensitive apps get a very high delay and bandwidth sensitive ones
are losing its share to the UDP backup.

<p align="center">
<img src="images/scenario-before.png">
</p>


After correctly configuring your TC at `router-eth3`, you should see something similar to the below image.
Both pings get min delay (and packets are not dropped). Zoom and Netflix traffic get a decent bandwidth share, with some priority on
Zoom (you can tune this as you wish). Finally, the backup traffic gets just a few KBs. This example shows what happens when all traffic
types are competing at the same time. Now, you can also play by removing some flows and see what happens. Are the requirements still met?

> Note: if you use `htb` classes, make sure that the guaranteed children bandwidth does not exceed the parents maximum!

<p align="center">
<img src="images/scenario-after.png">
</p>

### Prioritize h1 over h2 (optional extended version)

In the exercise above we asked you to peform a share distributon of traffic of the same type. How would you have to change your solution in order
to give `h1` a huge priority over `h2`. For example, `h1` should get 90% of the traffic for each traffic type in the case of both hosts sending and competing for bandwidth.

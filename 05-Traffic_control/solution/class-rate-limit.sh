#!/usr/bin/env bash

# example 1: ping has higher priority & and also fair?

# limit second link badnwdith with HTB

sudo ip netns exec router tc qdisc add dev router-eth3 root handle 1: htb default 50
sudo ip netns exec router tc class add dev router-eth3 parent 1: classid 1:1 htb rate 10Mbit ceil 10Mbit burst 15k

# HTB queues with prios

# high priority (ssh, etc) ICMP TOS 1
sudo ip netns exec router tc class add dev router-eth3 parent 1:1 classid 1:10 htb rate 500Kbit ceil 10Mbit prio 1 burst 15k cburst 15K
sudo ip netns exec router tc filter add dev router-eth3 parent 1: protocol ip prio 1 u32 match ip protocol 1 0xff match ip dsfield 1 0xff flowid 1:10

# voip ICMP TOS 2
sudo ip netns exec router tc class add dev router-eth3 parent 1:1 classid 1:20 htb rate 1Mbit ceil 10Mbit prio 2 burst 15k cburst 15K
sudo ip netns exec router tc filter add dev router-eth3 parent 1: protocol ip prio 1 u32 match ip protocol 1 0xff match ip dsfield 2 0xff flowid 1:20

# video calls (zoom) TCP 7070
sudo ip netns exec router tc class add dev router-eth3 parent 1:1 classid 1:30 htb rate 5Mbit ceil 10Mbit prio 3 burst 15k cburst 15K
sudo ip netns exec router tc filter add dev router-eth3 parent 1: protocol ip prio 1 u32 match ip protocol 6 0xff match ip dport 7070 0xffff flowid 1:30

# netflix TCP 6789
sudo ip netns exec router tc class add dev router-eth3 parent 1:1 classid 1:40 htb rate 3Mbit ceil 10Mbit prio 4 burst 15k cburst 15K
sudo ip netns exec router tc filter add dev router-eth3 parent 1: protocol ip prio 1 u32 match ip protocol 6 0xff match ip dport 6789 0xffff flowid 1:40

#default traffic
sudo ip netns exec router tc class add dev router-eth3 parent 1:1 classid 1:50 htb rate 450Kbit ceil 10Mbit prio 5 burst 15k cburst 15K

#backup UDP 5000
sudo ip netns exec router tc class add dev router-eth3 parent 1:1 classid 1:60 htb rate 50Kbit ceil 10Mbit prio 6 burst 15k cburst 15K
sudo ip netns exec router tc filter add dev router-eth3 parent 1: protocol ip prio 1 u32 match ip protocol 17 0xff match ip dport 5000 0xffff flowid 1:60


# Last qdisc
sudo ip netns exec router tc qdisc add dev router-eth3 parent 1:10 handle 10: sfq perturb 10 limit 64 quantum 10000
sudo ip netns exec router tc qdisc add dev router-eth3 parent 1:20 handle 20: sfq perturb 10 limit 64 quantum 10000
sudo ip netns exec router tc qdisc add dev router-eth3 parent 1:30 handle 30: sfq perturb 10 limit 64 quantum 10000
sudo ip netns exec router tc qdisc add dev router-eth3 parent 1:40 handle 40: sfq perturb 10 limit 64 quantum 10000
sudo ip netns exec router tc qdisc add dev router-eth3 parent 1:50 handle 50: sfq perturb 10 limit 64 quantum 10000
sudo ip netns exec router tc qdisc add dev router-eth3 parent 1:60 handle 60: sfq perturb 10 limit 64 quantum 10000
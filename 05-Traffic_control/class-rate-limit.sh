#!/usr/bin/env bash


# limit  link badnwdith with HTB
sudo ip netns exec router tc qdisc add dev router-eth3 root handle 1: htb default 1
sudo ip netns exec router tc class add dev router-eth3 parent 1: classid 1:1 htb rate 10Mbit ceil 10Mbit burst 15k

# TASK: define your qdiscs and classes such that the case requirements are met
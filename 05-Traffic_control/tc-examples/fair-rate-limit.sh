#!/usr/bin/env bash

# limit link badnwdith with HTB

sudo ip netns exec router tc qdisc add dev router-eth3 root handle 5:0 htb default 1
sudo ip netns exec router tc class add dev router-eth3 parent 5:0 classid 5:1 htb rate 10Mbit burst 15k

# add stochastic fair queue as leaf
sudo ip netns exec router tc qdisc add dev router-eth3 parent 5:1 handle 10: sfq perturb 10 limit 64 quantum 10000
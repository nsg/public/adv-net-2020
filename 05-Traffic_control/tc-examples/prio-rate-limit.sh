#!/usr/bin/env bash

# limit link badnwdith with HTB

sudo ip netns exec router tc qdisc add dev router-eth3 root handle 1: htb default 20
sudo ip netns exec router tc class add dev router-eth3 parent 1: classid 1:1 htb rate 10Mbit burst 15k

# childs with prios
sudo ip netns exec router tc class add dev router-eth3 parent 1:1 classid 1:10 htb rate 1Mbit ceil 10Mbit prio 1 burst 15k
sudo ip netns exec router tc class add dev router-eth3 parent 1:1 classid 1:20 htb rate 9Mbit ceil 10Mbit prio 2 burst 15k

# Last qdisc
sudo ip netns exec router tc qdisc add dev router-eth3 parent 1:10 handle 10: pfifo limit 100
sudo ip netns exec router tc qdisc add dev router-eth3 parent 1:20 handle 20: sfq perturb 10 limit 64 quantum 10000

# icmp marking and send to 1:10 which is htb prio 1
sudo ip netns exec router tc filter add dev router-eth3 parent 1: protocol ip prio 1 u32 match ip protocol 1 0xff flowid 1:10
#!/usr/bin/env bash

# example 1: Bandwidth rate limiting using HTB or TBF

# limit second link badnwdith with HTB
sudo ip netns exec router tc qdisc add dev router-eth3 root handle 5:0 htb default 1
sudo ip netns exec router tc class add dev router-eth3 parent 5:0 classid 5:1 htb rate 10Mbit burst 15k
sudo ip netns exec router tc qdisc add dev router-eth3 parent 5:1 handle 10: pfifo limit 1000

# Things you can play with:
# 1. UDP, TCP rate limiting
# 2. Combination of TCP and UDP, check it is not fair.
# 3. Combination UDP or TCP with a ping, and play with the pifo queue limit and see how it increases.
# 4. check packet drop difference when having small queues.

# ALTERNATIVE using TBF (classless)
#sudo ip netns exec h1 tc qdisc add dev h1-eth0 root tbf rate 10Mbit  burst 20k limit 15k
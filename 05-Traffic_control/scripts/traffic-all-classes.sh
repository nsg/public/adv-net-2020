#!/usr/bin/env bash

# Run: $ tmux 
# You must run this script in a tmux terminal

function topo {

    tmux kill-pane -a -t 0

    tmux split-window -h
    
    tmux select-pane -t 0
    tmux split-window -v -p 80

    tmux select-pane -t 1
    tmux split-window -v

    tmux select-pane -t 1
    tmux split-window -v

    tmux select-pane -t 3
    tmux split-window -v

    # vertical splits 

    tmux select-pane -t 5
    tmux split-window -v -p 80

    tmux select-pane -t 6
    tmux split-window -v

    tmux select-pane -t 6
    tmux split-window -v

    tmux select-pane -t 8
    tmux split-window -v

    tmux select-pane -t 0
    tmux send "scripts/clean_topo.sh" ENTER
    tmux send "scripts/run_topo.sh" ENTER

    # SET DEFAULT TC

    tmux send "sleep 1; ./class-rate-limit.sh" ENTER

    # high priority traffic
    tmux select-pane -t 1
    tmux send "sleep 2; sudo ip netns exec $1 bash" ENTER
    tmux send "sleep 3; ping 10.0.3.2 -i 0.1 -s 500 -Q 1" ENTER

    # Voip traffic
    tmux select-pane -t 6
    tmux send "sleep 2; sudo ip netns exec $1 bash" ENTER
    tmux send "sleep 3; ping 10.0.3.2 -i 0.01 -s 1500 -Q 2" ENTER

    # video call traffic

    tmux select-pane -t 7
    tmux send "sleep 2; sudo ip netns exec server bash" ENTER
    tmux send "sleep 3; iperf -s -p 7070 -i 1" ENTER


    tmux select-pane -t 2
    tmux send "sleep 2; sudo ip netns exec $1 bash" ENTER
    tmux send "sleep 4; iperf -c 10.0.3.2 -t 500 -i 3 -p 7070" ENTER

    # netflix traffic

    tmux select-pane -t 8
    tmux send "sleep 2; sudo ip netns exec server bash" ENTER
    tmux send "sleep 3; iperf -s -p 6789 -i 1" ENTER

    tmux select-pane -t 3
    tmux send "sleep 2; sudo ip netns exec $1 bash" ENTER
    tmux send "sleep 4; iperf -c 10.0.3.2 -t 500 -i 3 -p 6789" ENTER

    # backup traffic

    tmux select-pane -t 9
    tmux send "sleep 2; sudo ip netns exec server bash" ENTER
    tmux send "sleep 3; iperf -s -u -p 5000 -i 1" ENTER

    tmux select-pane -t 4
    tmux send "sleep 2; sudo ip netns exec $1 bash" ENTER
    tmux send "sleep 4; iperf -c 10.0.3.2 -t 500 -i 3 -u -b 10M -p 5000" ENTER

}

topo "h1"
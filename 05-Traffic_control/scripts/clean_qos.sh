#!/usr/bin/env bash

sudo ip netns exec h1 tc qdisc del root dev h1-eth1 
sudo ip netns exec h2 tc qdisc del root dev h2-eth1 

sudo ip netns exec router tc qdisc del root dev router-eth1
sudo ip netns exec router tc qdisc del root dev router-eth2
sudo ip netns exec router tc qdisc del root dev router-eth3

sudo ip netns exec server tc qdisc del root dev server-eth1
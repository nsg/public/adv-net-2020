#!/usr/bin/env bash

# Run: $ tmux 
# You must run this script in a tmux terminal

function topo {
    tmux kill-pane -a -t 0
    
    tmux split-window -h
    tmux select-pane -t 0
    tmux split-window -v

    tmux select-pane -t 2
    tmux split-window -v

    tmux select-pane -t 1
    tmux split-window -v

    tmux select-pane -t 4
    tmux split-window -v

    tmux select-pane -t 0
    tmux send "scripts/clean_topo.sh" ENTER
    tmux send "scripts/run_topo.sh" ENTER

    # SET DEFAULT TC
    tmux send "sleep 1; tc-examples/simple-rate-limit.sh" ENTER

    tmux select-pane -t 1
    tmux send "sleep 2; sudo ip netns exec h1 bash" ENTER

    tmux select-pane -t 2
    tmux send "sleep 2; sudo ip netns exec h2 bash" ENTER

    tmux select-pane -t 4
    tmux send "sleep 2; sudo ip netns exec server bash" ENTER

    tmux select-pane -t 5
    tmux send "sleep 2; sudo ip netns exec server bash" ENTER

    tmux select-pane -t 4
    tmux send "sleep 3; iperf -s -p 5001 -i 1" ENTER

    tmux select-pane -t 5
    tmux send "sleep 3; iperf -s -u -p 5002 -i 1" ENTER

    tmux select-pane -t 1
    tmux send "sleep 4; iperf -c 10.0.3.2 -t 500 -i 3 -p 5001" ENTER
    tmux select-pane -t 2
    tmux send "sleep 4;iperf -c 10.0.3.2 -t 500 -i 3 -u -b 15M -p 5002" ENTER
}

topo
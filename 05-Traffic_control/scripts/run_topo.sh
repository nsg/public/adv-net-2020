#!/usr/bin/env bash

# Create namespaces
sudo ip netns add h1
sudo ip netns add h2
sudo ip netns add server

sudo ip netns add router

# enable ip forward at router and isp
sudo ip netns exec router sysctl -w net.ipv4.ip_forward=1

# Bring the loopback interfaces up
sudo ip netns exec h1 ifconfig lo up
sudo ip netns exec h2 ifconfig lo up
sudo ip netns exec server ifconfig lo up
sudo ip netns exec router ifconfig lo up

# Create veth interfaces pairs
sudo ip link add h1-eth1 type veth peer name router-eth1
sudo ip link add h2-eth1 type veth peer name router-eth2
sudo ip link add router-eth3  type veth peer name server-eth1

#declare array of interfaces names
declare -a arr=("h1-eth1" "h2-eth1" "router-eth1" "router-eth2" "router-eth3" "server-eth1")

for intf in "${arr[@]}"
do
    #sudo ethtool --offload "$intf" tso off  &> /dev/null
    #sudo /sbin/ethtool -K "$intf" tso off gso off
    #sudo ip link set "$intf" mtu 9500
    sudo sysctl net.ipv6.conf.${intf}.disable_ipv6=1
    :
done

# Move interface sides to host
sudo ip link set h1-eth1 netns h1
sudo ip link set h2-eth1 netns h2

# home router
sudo ip link set router-eth1 netns router
sudo ip link set router-eth2 netns router
sudo ip link set router-eth3 netns router


# server
sudo ip link set server-eth1 netns server


#  Set interface ips
sudo ip netns exec h1 ifconfig h1-eth1 hw ether 00:00:0a:00:01:02 10.0.1.2/24 up
sudo ip netns exec h2 ifconfig h2-eth1 hw ether 00:00:0a:00:02:02 10.0.2.2/24 up

# router
sudo ip netns exec router ifconfig router-eth1 hw ether 00:00:0a:00:01:01 10.0.1.1/24 up
sudo ip netns exec router ifconfig router-eth2 hw ether 00:00:0a:00:02:01 10.0.2.1/24 up
sudo ip netns exec router ifconfig router-eth3 hw ether 00:00:0a:00:03:01 10.0.3.1/24 up

# server
sudo ip netns exec server ifconfig server-eth1 hw ether 00:00:0a:00:03:02 10.0.3.2/24 up


# gateways 
sudo ip netns exec h1 route add default gw 10.0.1.1 h1-eth1
sudo ip netns exec h2 route add default gw 10.0.2.1 h2-eth1
sudo ip netns exec server route add default gw 10.0.3.1 server-eth1
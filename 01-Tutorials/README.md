# P4 Tutorial

## Introduction

Welcome to the first exercise. This week, we start learning P4 and getting proficient in P4 programming.

The P4 exercises serve two purposes:
* By programming P4, we hands-on understand specific challenges in the Internet. As an example, we will program MPLS or RSVP forwarding in P4 and while doing this we will comprehend the concepts better.
* By following and practicing these exercises, we prepare for the final project, which comprises a meaningful portion (30%) of the final grade.

Let's begin with an introduction to our exercise environment, programming framework, and necessary tools. Then, we can already start coding the cornerstones of the Internet!

### Exercise Planning

What we envisioned so far is as follows (please let us know if you have a better idea): 

* The responsible host TA of the week presents the exercise,
* Then, general questions are discussed in the main room,
* If you have a personal question (you need to indicate that), we assign you to a break-out room. There, a TA assists you privately, when finished, parties come back to the main room.
* After the exercise session, you can always visit [Moodle](https://moodle-app2.let.ethz.ch/mod/forum/view.php?f=22500), or use our Slack workspace for further questions.

Before proceeding, please open:
* [Course Gitlab Repository](https://gitlab.ethz.ch/nsg/public/adv-net-2020)
* [P4-Learning Repository](https://github.com/nsg-ethz/p4-learning)

in your browsers.

## Development Tools

### Mininet Environment

Mininet creates a realistic virtual network, running real kernel, switch, and application code, on a single machine.

In our exercises, thanks to [P4 Utils](#p4-utils), most of the time we will not directly interact with Mininet, since automated scripts will build the necessary topology for us. 
When we need to know specific Mininet CLI commands, they will be introduced in the exercise.

Nevertheless, we recommend you take a look at [here](https://github.com/nsg-ethz/p4-learning/tree/master/documentation/mininetless); an example that shows how to run a small virtual network topology without using Mininet. This will let you understand what Mininet does behind the curtain.

In short, Mininet creates hosts using Unix namespaces and interconnects them using virtual interfaces and BMv2 switches. Linux namespaces provide a network stack for all the processes running within a specific namespace. Thus, network virtualization tools such as Mininet uses Linux network namespaces to instantiate isolated nodes in a topology (hosts, switches, routers, etc).

### BMv2 Switch

Behavioral Model version 2 (BMv2) is the second version of the reference P4 software switch. Please refer to the [documentation](https://github.com/p4lang/behavioral-model) for the details.

### Control Plane

After we build and run the BMv2 software switch, we can use the Simple Switch CLI to configure the switch and populate match-action tables.

To learn more about the Control Plane and its functions, please refer [here](https://github.com/nsg-ethz/p4-learning/blob/master/documentation/control-plane.md).

### Scapy

Scapy is a packet manipulation library written in Python. Please take a look at [here](https://github.com/nsg-ethz/p4-learning/blob/master/documentation/scapy.md) for further information.

We will use Scapy for packet creation and capture at end hosts in our exercises.

### Debugging and Troubleshooting

Monitoring traffic can be a very powerful tool when debugging your P4 program. 

To sniff the traffic that is going through an interface, we will use `tcpdump`. If you are interested in other similar tools or want to learn more about debugging and troubleshooting, please refer [here](https://github.com/nsg-ethz/p4-learning/blob/master/documentation/debugging-and-troubleshooting.md).

To capture binary-level traffic with `tcpdump` run:

```bash
sudo tcpdump -XX -i <interface_name>
```

### P4-Utils

P4-Utils is an extension to Mininet that makes P4 networks easier to build, run, and debug. Please refer to the [documentation](https://github.com/nsg-ethz/p4-utils) for the details.

### p4app.json

`p4app.json` describes the topology that we want to create with the help of Mininet and the P4-Utils package.

To create the topology described in `p4app.json`, you just have to call `p4run`, which by default will check if the file `p4app.json` exists in the path:

   ```bash
   sudo p4run
   ```
This will call a Python script that parses the configuration file, creates a virtual network of hosts and p4 switches using Mininet, compiles the P4 program, and loads it in the switch.

You can find the p4-utils documentation [here](https://github.com/nsg-ethz/p4-utils).

After running `p4run` you will get the Mininet CLI prompt (you can find some documentation about the CLI [here](https://github.com/nsg-ethz/p4-learning/blob/master/documentation/control-plane.md))

### P4 References

1. [P4 Spec](https://p4.org/p4-spec/): P4 Language and Related Specifications
2. [P4-guide](https://github.com/jafingerhut/p4-guide): Repository that contains a lot of useful information, examples, and tests of best practices, tips, and tricks.
3. [P4 Tutorials](https://github.com/p4lang/tutorials): Official P4 Tutorials

## First Week Exercises

Let's have a tour in your VMs...

Go down to your home directory:

```bash
p4@Advnet:/home$
```

Then,

```bash
p4@Advnet:/home$ cd p4/
p4@Advnet:~$ cd p4-tools/
p4@Advnet:~/p4-tools$ cd p4-learning/
p4@Advnet:~/p4-tools/p4-learning$
```

`~/p4-learning/` is our local copy of the [P4-Learning Repository](https://github.com/nsg-ethz/p4-learning).

In `~/p4-learning/documentation/` folder you will find the necessary documentation, and we will point to that folder in `README` of our exercises. Please go through them later on by yourself.

In `~/p4-learning/examples/` folder you will find the generic examples and their solutions that we put together for hands-on P4 learning. We advice you to practice them when you have time.

In `~/p4-learning/exercise/` folder you will find the previous years' exercises and their solutions. This serves the same purpose with the `../examples/` folder that we explained above. Other than that, this week for our tutorials, we will use some of the exercises in this folder.

After the Tutorials (the first week), we will have a new exercise for each week. These will be updated in the coming weeks, so you need to sync (`git pull`) your VMs with the [Course Gitlab Repository](https://gitlab.ethz.ch/nsg/lectures/lec_advnet/exercise/2020/-/tree/master).

Now, go down to your home directory once more:

```bash
p4@Advnet:/home$
```

```bash
p4@Advnet:/home$ cd
p4@Advnet:~$ git clone https://gitlab.ethz.ch/nsg/public/adv-net-2020.git
Cloning into 'adv-net-2020'...
Username for 'https://gitlab.ethz.ch': your_nethz_name
Password for 'https://your_nethz_name@gitlab.ethz.ch':
remote: Enumerating objects: 25, done.
remote: Counting objects: 100% (25/25), done.
remote: Compressing objects: 100% (25/25), done.
remote: Total 439 (delta 10), reused 0 (delta 0), pack-reused 414
Receiving objects: 100% (439/439), 962.42 KiB | 0 bytes/s, done.
Resolving deltas: 100% (183/183), done.
Checking connectivity... done.
```

This folder named `adv_net_2020` is your local copy of the [Course Gitlab Repository](https://gitlab.ethz.ch/nsg/lectures/lec_advnet/exercise/2020/-/tree/master), and **you need to pull this repository every week before the exercises since it will be updated by the TAs.**

As the final step, let's start the Tutorials:

#### Tutorial 1: Reflector

In this first exercise, we will program and build a P4 switch that bounces back the packets that it receives. By doing that, we will also learn to create a simple topology.

Please get into the directory as shown below:

```bash
p4@Advnet:~$ cd p4-tools/p4-learning/exercises/01-Reflector/
```

In that directory, you will find a `README` specific to that exercise. Follow the `README` to solve the exercise ([here](https://github.com/nsg-ethz/p4-learning/blob/master/exercises/01-Reflector/README.md) is the online copy).

#### Tutorial 2: Repeater

In this exercise, we will program and build a simple P4 switch that will forward packets between two hosts.

```bash
p4@Advnet:~$ cd p4-tools/p4-learning/exercises/02-Repeaters/
```

In that directory, you will find a `README` specific to that exercise. Follow the `README` to solve the exercise ([here](https://github.com/nsg-ethz/p4-learning/blob/master/exercises/02-Repeater/README.md) is the online copy).

#### Tutorial 3: L2 Basic Forwarding

In this exercise, we will implement a basic layer 2 forwarding switch. The switch will statically map MAC addresses to ports and will use this mapping for forwarding.

```bash
p4@Advnet:~$ cd p4-tools/p4-learning/exercises/03-L2_Basic_forwarding/
```

In that directory, you will find a `README` specific to that exercise. Follow the `README` to solve the exercise ([here](https://github.com/nsg-ethz/p4-learning/blob/master/exercises/03-L2_Basic_forwarding/README.md) is the online copy).

#### Tutorial 4: L2 Flooding

In the previous exercise we implemented an L2 forwarding switch that forwards packets based on the static MAC addresses. In this exercise, we will implement a switch that can also broadcast packets in case the forwarding information is unknown.

```bash
p4@Advnet:~$ cd p4-tools/p4-learning/exercises/03-L2_Flooding/
```

In that directory, you will find a `README` specific to that exercise. Follow the `README` to solve the exercise ([here](https://github.com/nsg-ethz/p4-learning/tree/master/exercises/03-L2_Flooding) is the online copy).

#### Tutorial 5: L2 learning

This is the last exercise of our Basic L2 Switch series. In this exercise, we will make the switch a bit smarter and add the capability of autonomously learning MAC addresses to port mappings.

```bash
p4@Advnet:~$ cd p4-tools/p4-learning/exercises/04-L2_Learning/
```

In that directory, you will find a `README` specific to that exercise. Follow the `README` to solve the exercise ([here](https://github.com/nsg-ethz/p4-learning/tree/master/exercises/04-L2_Learning) is the online copy).
